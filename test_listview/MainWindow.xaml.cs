﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace test_listview
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<TestObject> lto = new List<TestObject>()
        {
            new TestObject()
            {
                foo = true,
                bar = "test",
                baz = TestObject.priorité.haute
            },
            new TestObject()
            {
                foo = false,
                bar = "test2",
                baz = TestObject.priorité.urgent
            },
        };

        class TestObject
        {
            public bool foo;
            public string bar;
            public priorité baz;
            public enum priorité
            {
                normale,
                haute,
                urgent
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            foreach(TestObject to in lto)
            {
                //déclarer un ListView appelé LVTest dans le xaml
                ListViewItem lvi = new ListViewItem();
                lvi.Content = to.bar;
                switch (to.baz)
                {
                    case TestObject.priorité.haute:
                        lvi.Background = Brushes.Yellow;
                        break;                    
                    case TestObject.priorité.urgent:
                        lvi.Background = Brushes.Red;
                        break;
                }
                //créer un Listview avec une propriété x:Name de LVTest pour utiliser ce code.
                LVTest.Items.Add(lvi);
            }
        }
    }
}
